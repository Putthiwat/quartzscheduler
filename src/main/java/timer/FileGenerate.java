package timer;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.io.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileGenerate implements Job {
    int num = 0;
    DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyyMMdd");
    DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
    LocalDateTime now = LocalDateTime.now();
    List<String> stringList;

    public void execute(JobExecutionContext context) {
        OracleLink.Query();

        try {
            PrintWriter temp = new PrintWriter
                    ("genFile/Data0.txt");
            temp.println("0000|" +
                    "EnforcementData.txt|" +
                    date.format(now) + "|" +
                    time.format(now) + "|" +
                    "2|EXACT");
            temp.flush();
            temp.close();
        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        }

        while(num < OracleLink.row) {
            // Get table data
            stringList = Stream.of
                    (
                            "1000|" +
                                    OracleLink.issDate.get(num) + "|" +
                                    OracleLink.cusNo.get(num) + "|" +
                                    OracleLink.cusId.get(num) + "||" +
                                    OracleLink.cusName.get(num) + "|" +
                                    OracleLink.cusAddr.get(num) + "||||" +
                                    OracleLink.carNo1.get(num) + " " +
                                    OracleLink.carNo2.get(num) + "|" +
                                    OracleLink.carPro.get(num) + "|" +
                                    OracleLink.dueDate.get(num) + "|" +
                                    OracleLink.dueTime.get(num) + "|" +
                                    OracleLink.serveId.get(num) + "|" +
                                    OracleLink.billFee.get(num) + "|" +
                                    OracleLink.billColl.get(num) + "|" +
                                    OracleLink.feeColl.get(num) + "|" +
                                    OracleLink.inDate.get(num) + "|" +
                                    OracleLink.billFine.get(num) + "|" +
                                    OracleLink.fecoFine.get(num) + "|||\n" +

                                    "2000|" +
                                    OracleLink.issDate.get(num) + "|" +
                                    OracleLink.cusNo.get(num) + "|" +
                                    OracleLink.cusId.get(num) + "|" +
                                    OracleLink.cusName.get(num) + "|" +
                                    OracleLink.cusAddr.get(num) + "||||" +
                                    OracleLink.carNo1.get(num) + " " +
                                    OracleLink.carNo2.get(num) + "|" +
                                    OracleLink.carPro.get(num) + "||" +
                                    OracleLink.invNo.get(num) + "|" +
                                    OracleLink.invRef.get(num) + "|" +
                                    OracleLink.billTotal.get(num) + "|" +
                                    OracleLink.inDate.get(num) + "|" +
                                    OracleLink.billFee.get(num) + "|" +
                                    OracleLink.billColl.get(num) + "|" +
                                    OracleLink.billFine.get(num) + "|" +
                                    OracleLink.billOpera.get(num) + "|" +
                                    OracleLink.billTotal.get(num) + "|" +
                                    OracleLink.qrCode.get(num) + "|||" +
                                    OracleLink.billVat.get(num) + "\n" +

                                    "3000|" +
                                    OracleLink.issDate.get(num) + "|" +
                                    OracleLink.cusId.get(num) + "|" +
                                    OracleLink.carNo1.get(num) + " " +
                                    OracleLink.carNo2.get(num) + "|" +
                                    OracleLink.carPro.get(num) + "|" +
                                    OracleLink.tranDate.get(num) + "|" +
                                    OracleLink.tranTime.get(num) + "|" +
                                    OracleLink.hqName.get(num) + "|" +
                                    OracleLink.plazaName.get(num) + "|" +
                                    OracleLink.laneName.get(num) + "|" +
                                    OracleLink.billFee.get(num) + "\n" +

                                    "4000|" +
                                    OracleLink.lettDate.get(num) + "|" +
                                    OracleLink.cusNo.get(num) + "|" +
                                    OracleLink.cusId.get(num) + "|" +
                                    OracleLink.issDate.get(num) + "|" +
                                    OracleLink.issTime.get(num) + "|" +
                                    OracleLink.notiType.get(num) + "|" +
                                    "mflow@doh.go.th" + "|" +
                                    OracleLink.notiMail.get(num) + "|||||\n" +

                                    "8888|End of Customer"
                    ).collect(Collectors.toList());
            String textContent = stringList.toString().replace("[", "").replace("]", "");

            try {
                // Write new file
                PrintWriter pw1 = new PrintWriter
                        ("genFile/Data" + (num + 1) + ".txt");

                // Read previous file
                BufferedReader br1 = new BufferedReader
                        (new FileReader("genFile/Data" + num + ".txt"));
                String line = br1.readLine();

                // Import previous content
                while (line != null) {
                    pw1.println(line);
                    line = br1.readLine();
                }

                pw1.println(textContent);
                pw1.flush();
                br1.close();
                pw1.close();
                Files.deleteIfExists(Paths.get("genFile/Data" + num + ".txt"));
            } catch (IOException ex) {
                ex.printStackTrace(System.out);
            }

            System.out.println("\n" + OracleLink.cusId.get(num) + "\n");
            num = num + 1;
        }

        num = num - 1;

        try {
            // Write new file
            PrintWriter pw2 = new PrintWriter
                    ("genFile/EnforcementData" + OracleLink.issDate.get(num) + ".txt");

            // Read previous file
            BufferedReader br2 = new BufferedReader
                    (new FileReader("genFile/Data" + (num + 1) + ".txt"));
            String line = br2.readLine();

            // Import previous content
            while (line != null) {
                pw2.println(line);
                line = br2.readLine();
            }

            int totalFee = 0;

            for (int i = 0; i < num; i++) {
                totalFee = totalFee + Integer.parseInt(OracleLink.billFee.get(i));
            }

            int totalColl = 0;

            for (int j = 0; j < num; j++) {
                totalColl = totalColl + Integer.parseInt(OracleLink.billColl.get(j));
            }

            int totalFine = 0;

            for (int k = 0; k < num; k++) {
                totalFine = totalFine + Integer.parseInt(OracleLink.billFine.get(k));
            }

            pw2.println("9999|" +
                    "End of File|" +
                    date.format(now) + "|" +
                    totalFee + "|" +
                    totalColl + "|" +
                    totalFine + "|" +
                    (num + 1) + "|" +
                    (num + 1));
            pw2.flush();
            br2.close();
            pw2.close();
            Files.deleteIfExists(Paths.get("genFile/Data" + (num + 1) + ".txt"));
        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        }
    }
}