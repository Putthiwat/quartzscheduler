package timer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OracleLink {
    public static List<String> cusAddr = new ArrayList<>();
    public static List<String> issDate = new ArrayList<>();
    public static List<String> cusType = new ArrayList<>();
    public static List<String> cusId = new ArrayList<>();
    public static List<String> cusName = new ArrayList<>();
    public static List<String> carNo1 = new ArrayList<>();
    public static List<String> carNo2 = new ArrayList<>();
    public static List<String> carPro = new ArrayList<>();
    public static List<String> dueDate = new ArrayList<>();
    public static List<String> dueTime = new ArrayList<>();
    public static List<String> serveId = new ArrayList<>();
    public static List<String> billFee = new ArrayList<>();
    public static List<String> billColl = new ArrayList<>();
    public static List<String> feeColl = new ArrayList<>();
    public static List<String> inDate = new ArrayList<>();
    public static List<String> billFine = new ArrayList<>();
    public static List<String> fecoFine = new ArrayList<>();
    public static List<String> invNo = new ArrayList<>();
    public static List<String> invRef = new ArrayList<>();
    public static List<String> billTotal = new ArrayList<>();
    public static List<String> billOpera = new ArrayList<>();
    public static List<String> billVat = new ArrayList<>();
    public static List<String> tranDate = new ArrayList<>();
    public static List<String> tranTime = new ArrayList<>();
    public static List<String> hqName = new ArrayList<>();
    public static List<String> plazaName = new ArrayList<>();
    public static List<String> laneName = new ArrayList<>();
    public static List<String> notiType = new ArrayList<>();
    public static List<String> notiMail = new ArrayList<>();
    public static List<String> issTime = new ArrayList<>();
    public static List<String> lettDate = new ArrayList<>();
    public static List<String> qrCode = new ArrayList<>();
    public static List<String> cusNo = new ArrayList<>();
    public static int row = 0;

    public static void Query() {
        try {
            // Set Oracle connection
            Class.forName("oracle.jdbc.OracleDriver");
            Connection con = DriverManager.getConnection

                    // url | user | password
                            (
                                    "jdbc:oracle:thin:@192.168.0.61:1522:edwdevdb1",
                                    "appworks",
                                    "appworks"
                            );

            Statement stmt = con.createStatement();

            // Query table data
            ResultSet rs = stmt.executeQuery
                    (
                            "select a.ADDRESS, to_char(a.ISSUE_DATE, 'yyyymmdd'), \n" +
                                    "a.CUSTOMER_TYPE, a.CUSTOMER_ID, a.FULL_NAME, b.PLATE1, b.PLATE2, \n" +
                                    "c.DESCRIPTION, to_char(a.DUE_DATE + 12, 'yyyymmdd'), \n" +
                                    "to_char(a.DUE_DATE + 12, 'hh:mm'), a.SERVICE_PROVIDER, a.FEE_AMOUNT, \n" +
                                    "a.COLLECTION_AMOUNT, (a.FEE_AMOUNT + a.COLLECTION_AMOUNT), \n" +
                                    "to_char(a.DUE_DATE, 'yyyymmdd'), a.FINE_AMOUNT, \n" +
                                    "(a.FEE_AMOUNT + a.COLLECTION_AMOUNT + a.FINE_AMOUNT), a.INVOICE_NO, \n" +
                                    "a.INVOICE_NO_REF, a.TOTAL_AMOUNT, a.OPERATION_FEE, a.VAT, \n" +
                                    "to_char(b.TRANSACTION_DATE, 'yyyymmdd'), to_char(b.TRANSACTION_DATE, 'hh:mm'), \n" +
                                    "d.NAME, e.NAME, f.NAME, g.TYPE, g.DESCRIPTION, to_char(a.ISSUE_DATE, 'hh:mm'), \n" +
                                    "to_char(a.ISSUE_DATE + 3, 'yyyymmdd'), a.QR_CODE, \n" +

                                    "case when h.CUSTOMER_TYPE = 'Individual' then '1' \n" +
                                    "else '2' end as CUST_NO \n" +

                                    "from MF_ENFORCEMENT a \n" +

                                    "inner join MF_ENFORCEMENT_DETAIL b on a.INVOICE_NO = b.INVOICE_NO \n" +
                                    "inner join MF_ENFORCEMENT_MASTER_V_OFFICE c on b.PROVINCE = c.CODE \n" +
                                    "inner join MF_ENFORCEMENT_MASTER_HQ d on b.HQ_CODE = d.CODE \n" +
                                    "inner join MF_ENFORCEMENT_MASTER_PLAZA e on b.PLAZA_CODE = e.CODE \n" +
                                    "inner join MF_ENFORCEMENT_MASTER_LANE f on b.LANE_CODE = f.CODE \n" +
                                    "inner join MF_CUST_NOTIFICATION_HISTORY g on a.CUSTOMER_ID = g.CUSTOMER_ID \n" +
                                    "inner join MF_CUST_INFO h on a.CUSTOMER_ID = h.CUSTOMER_ID \n" +

                                    "where a.STATUS = 'PAYMENT_WAITING' \n" +
                                    "and a.CUSTOMER_ID is not null \n" +
                                    "and a.TOTAL_AMOUNT <> 0 \n" +
                                    "and a.ISSUE_DATE = TO_DATE('20211007','yyyyMMdd')"
                    );

            while (rs.next()) {
                cusAddr.add(rs.getString(1));
                issDate.add(rs.getString(2));
                cusType.add(rs.getString(3));
                cusId.add(rs.getString(4));
                cusName.add(rs.getString(5));
                carNo1.add(rs.getString(6));
                carNo2.add(rs.getString(7));
                carPro.add(rs.getString(8));
                dueDate.add(rs.getString(9));
                dueTime.add(rs.getString(10));
                serveId.add(rs.getString(11));
                billFee.add(rs.getString(12));
                billColl.add(rs.getString(13));
                feeColl.add(rs.getString(14));
                inDate.add(rs.getString(15));
                billFine.add(rs.getString(16));
                fecoFine.add(rs.getString(17));
                invNo.add(rs.getString(18));
                invRef.add(rs.getString(19));
                billTotal.add(rs.getString(20));
                billOpera.add(rs.getString(21));
                billVat.add(rs.getString(22));
                tranDate.add(rs.getString(23));
                tranTime.add(rs.getString(24));
                hqName.add(rs.getString(25));
                plazaName.add(rs.getString(26));
                laneName.add(rs.getString(27));
                notiType.add(rs.getString(28));
                notiMail.add(rs.getString(29));
                issTime.add(rs.getString(30));
                lettDate.add(rs.getString(31));
                qrCode.add(rs.getString(32));
                cusNo.add(rs.getString(33));
                row = row + 1;
            }

            con.close();
        } catch (Exception e) {
            System.out.println();
        }
    }
}