package timer;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class ScheduleConfig {
    public static void main(String[] args) {
        try {
            // Set job details.
            JobDetail job = JobBuilder.newJob(FileGenerate.class)
                    .withIdentity("newJob", "group1")
                    .build();

            // Set scheduler timings.
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity("cronTrigger", "group1")

                    // second | minute | hour | day(month) | month | day(week) | year
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 53 9 * * ? *"))
                    .build();

            // Execute the job.
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}